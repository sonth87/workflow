import { useEffect, useRef } from "react";
import BpmnViewer from "bpmn-js/lib/NavigatedViewer";
import "bpmn-js/dist/assets/diagram-js.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn.css";
import "bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css";
import pizzaDiagram from "../src/components/elementBpmXml.bpmn";
import $ from "jquery";

function VideoPlayer() {
  useEffect(() => {
    // viewer instance
    var bpmnViewer = new BpmnViewer({
      container: "#canvas",
      keyboard: {
        bindTo: document,
      },
      // additionalModules: [EmbeddedComments],
    });

    async function openDiagram(diagram) {
      try {
        await bpmnViewer.importXML(diagram);

        bpmnViewer.get("canvas").zoom("fit-viewport");
      } catch (err) {
        alert("could not import BPMN 2.0 XML, see console");
        return console.log("could not import BPMN 2.0 XML", err);
      }
    }

    // file save handling

    var $download = $("[data-download]");

    async function serialize() {}

    bpmnViewer.on("comments.updated", serialize);
    bpmnViewer.on("commandStack.changed", serialize);

    bpmnViewer.on("canvas.click", function () {
      bpmnViewer.get("comments").collapseAll();
    });

    // file open handling

    var $file = $("[data-open-file]");

    function readFile(file, done) {
      if (!file) {
        return done(new Error("no file chosen"));
      }

      var reader = new FileReader();

      reader.onload = function (e) {
        done(null, e.target.result);
      };

      reader.readAsText(file);
    }

    $file.on("change", function () {
      readFile(this.files[0], function (err, xml) {
        if (err) {
          alert("could not read file, see console");
          return console.error("could not read file", err);
        }

        openDiagram(xml);
      });
    });

    // we use stringify to inline a simple BPMN diagram
    openDiagram(pizzaDiagram);

    // file drag / drop ///////////////////////

    function openFile(file, callback) {
      // check file api availability
      if (!window.FileReader) {
        return window.alert(
          "Looks like you use an older browser that does not support drag and drop. " +
            "Try using a modern browser such as Chrome, Firefox or Internet Explorer > 10."
        );
      }

      // no file chosen
      if (!file) {
        return;
      }

      var reader = new FileReader();

      reader.onload = function (e) {
        var xml = e.target.result;

        callback(xml);
      };

      reader.readAsText(file);
    }

    (function onFileDrop(container, callback) {
      function handleFileSelect(e) {
        e.stopPropagation();
        e.preventDefault();

        var files = e.dataTransfer.files;
        openFile(files[0], callback);
      }

      function handleDragOver(e) {
        e.stopPropagation();
        e.preventDefault();

        e.dataTransfer.dropEffect = "copy"; // Explicitly show this is a copy.
      }

      container.get(0).addEventListener("dragover", handleDragOver, false);
      container.get(0).addEventListener("drop", handleFileSelect, false);
    })($("body"), openDiagram);
  }, []);

  return (
    <>
      <div className="header">
        <h1>BPMN 2.0 comments</h1>

        <div className="toolbar">
          <div className="entry">
            <a href data-download download="pizza-collaboration-annotated.bpmn">
              Download
            </a>
          </div>

          <div className="entry">
            <input type="file" data-open-file value="" />
          </div>
        </div>
      </div>

      <div id="canvas"></div>
    </>
  );
}

const diagramViewer = () => {
  return <VideoPlayer />;
};

export default diagramViewer;
