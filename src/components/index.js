import React, { useEffect } from 'react'
import BpmnXml from '../components/bpmnxml.bpmn'
import BpmnModeler from "bpmn-js/lib/Modeler";

const ModelerCreator = () => {
    const diagramModeler = new BpmnModeler({
        container: "#js-canvas"
    });

    useEffect(() => {
        // console.log("diagramModeler: ", diagramModeler);

        diagramModeler.importXML(BpmnXml, err => {
            if(err) console.log("err", err);
            else {
                console.log("first")
            }
        })
    }, [])
  return (
    <div id="js-canvas" />
  )
}

export default ModelerCreator